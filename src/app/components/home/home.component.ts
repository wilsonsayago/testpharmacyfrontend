import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import * as $ from 'jquery';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { Pharmacy } from '../../entity/pharmacy';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  displayedColumns: String[] = ['localName', 'localDirection', 'localPhone', 'localLat', 'localLng'];
  dataSource: MatTableDataSource<Pharmacy> = new MatTableDataSource([]) ;
  pharmacies : Pharmacy[] = [];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  lists:string = "";
  communeId : number = 0;
  nameLocal : String = null;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getCommunes().subscribe((data: any)=>{
      $("#miselect").append(data['communes']);
    });
    this.apiService.getAllPharmacies(0, null).subscribe((data: any)=>{
      this.pharmacies = data['pharmacies'];
      this.dataSource.data = this.pharmacies;
      this.dataSource.paginator = this.paginator;
    });
  }
  search(){
    this.apiService.getAllPharmacies(this.communeId, this.nameLocal).subscribe((data: any)=>{
      this.pharmacies = data['pharmacies'];
      this.dataSource.data = this.pharmacies;
      this.dataSource.paginator = this.paginator;
    });
  }
}
