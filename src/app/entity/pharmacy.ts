export class Pharmacy {
  localName: string;
  localDirection: string;
  localPhone: string;
  localLat: string;
  localLng: string;
}
