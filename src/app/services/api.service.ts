import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = "http://localhost:8080";

  constructor(private httpClient: HttpClient) { }

  public getCommunes(){
		return this.httpClient.get(this.SERVER_URL + "/communes");
	}

	public getAllPharmacies(communeId: number, local: String) {
    var params = "?communeId=" + communeId;
    if (local != null){
      params += "&local=" + local;
    }
    return this.httpClient.get(this.SERVER_URL + "/pharmacy" + params);
	}
}
